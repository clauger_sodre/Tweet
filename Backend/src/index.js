const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("../swagger/swagger_output.json");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  cors({
    origin: "*"
  })
);

const sequelize = require("./utils/database.js");
const router = require("./routes/router.js");
const association = require("./models/associations");
const port = process.env.PORT || 3001;

(async () => {
  try {
    association.association();
    await sequelize.sync(
      { alter: true } // Reset db every time
    );
    app.listen(port, function () {
      console.log("listen on port " + port);
    });
  } catch (error) {
    console.log(error);
  }
})();

app.use("/", router);

// Swagger
app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));
