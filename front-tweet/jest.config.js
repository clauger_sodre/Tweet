const nextJest = require("next/jest");

const config = require("./jest-en")
const createJestConfig = nextJest({
  dir: "./",
});
const customJestConfig = {
  moduleDirectories: ["node_modules", "<rootDir>/"],
  testEnvironment: config,
};


module.exports = createJestConfig(customJestConfig)
//https://blog.logrocket.com/testing-next-js-apps-jest/