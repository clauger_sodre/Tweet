import { useEffect, useState } from "react";
import style from "../styles/footer.module.css";

export default function Footer() {
  const [yearDate, setYearDate] = useState("");
  useEffect(() => {
    setYearDate(
      new Date()
        .toLocaleString("pt-BR", { timeZone: "UTC" })
        .split(",")[0]
        .split("/")[2] || "2021"
    );
  },[]);
  return (
    <div className={style.footer}>
      <p > Created by Company {yearDate}</p>
    </div>
  );
}
