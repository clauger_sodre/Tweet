import style from "@/styles/homePage.module.css";
import PostCard from "./postCard";
import Post from "@/models/interfaces/postInterface";

export default function customBody({ props: { posts } }: any) {
  return (
    <div>
      <main className={style.titleHomePage}>
        <>
          <p>Your Freshest Tweets&nbsp;</p>
          {posts && posts.map((e: Post) =><div key={e.id}> <PostCard {...e} /></div>)}
        </>
      </main>
    </div>
  );
}
