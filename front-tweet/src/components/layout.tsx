import Header from "@/components/header";
import Footer from "@/components/footer";
import Navbar from "@/components/navbar";
import style from "@/styles/layout.module.css";
import LeftBar from "./leftbar";
import BootstrapConfig from "./bootstrapConfig";
export default function Layout({ children }: any) {
  return (
    <div className={style.layout}>
      <BootstrapConfig />
      <div className={style.Header}>
        <Header />
      </div>
      <div className={style.NavBar}>
        <Navbar />
      </div>
      <div className={style.Container}>
        <div className={style.LeftBar}>
          <LeftBar />
        </div>
        <div className={style.Body}>{children}</div>
      </div>
      <div className={style.Footer}>
        <Footer />
      </div>
    </div>
  );
}
