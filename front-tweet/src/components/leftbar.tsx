import { StateRedux } from "@/store/type-redux";
import { useSelector } from "react-redux";

export default function LeftBar() {
  const numberOfPosts = useSelector((state: StateRedux) => state.numberOfPosts);
  const userData = useSelector((state: StateRedux) => state.userData);

  return (
    <>
      <div>{`Hello  ${userData.username}`}</div>
      <div>{`Number of Tweets 🐦 ${numberOfPosts}`}</div>
      <div>{`🗓️ joined at ${userData.joined}`}</div>
    </>
  );
}
