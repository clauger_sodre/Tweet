import Image from "next/image";
import Style from "@/styles/postCard.module.css";
import Post from "@/models/interfaces/postInterface";
import { RecursivePost } from "./recursivePost";

export default function PostCard({
  id,
  userId,
  data,
  message,
  reposts,
  quotes,
}: Post) {
  return (
    <>
      <div className={`${Style.Card} card-img-justify`} key={id+"POST"}>
        <Image
          className={`${Style.borderCircle} card-img-justify`}
          src="/images/ltweet.jpg"
          height={244}
          width={274}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">User {userId}</h5>
          <h5 className="card-title">{data.toString().split("T")[0]}</h5>
          <p className={`${Style.CardText}card-text`}>{message}</p>
        </div>
        {quotes.length > 0 && (
          <>
            <RecursivePost parent={quotes} name="Quote" />
          </>
        )}
        {reposts.length > 0 && (
          <div>
            <RecursivePost parent={reposts} name="Repost" />
          </div>
        )}
      </div>
    </>
  );
}
