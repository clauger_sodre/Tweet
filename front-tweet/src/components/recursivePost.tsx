import Style from "@/styles/postCard.module.css";
import Image from "next/image";
export function RecursivePost({ parent, name }: any) {
  if (typeof parent !== "undefined") {
    if (!Array.isArray(parent)) {
      const newArray = [];
      newArray.push(parent);
      parent = newArray;
    }
  }
  return (
    <>
      {parent?.length > 0 &&
        parent.map((parentes: any) => {
          return (
            <div key={parentes.id + name}>
              <div
                className={
                  name === "Quote"
                    ? `${Style.CardQuote} card-img-justify`
                    : `${Style.CardRepost} card-img-justify`
                }
              >
                <Image
                  className={
                    name === "Repost"
                      ? `${Style.Repost} card-img-justify`
                      : `${Style.Quote} card-img-justify`
                  }
                  src="/images/ltweet.jpg"
                  height={144}
                  width={174}
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">{`${name} ${parentes.id}`}</h5>
                  <h5 className="card-title">
                    {parentes.data.toString().split("T")[0]}
                  </h5>
                  <p className={`${Style.CardText}card-text`}>
                    {parentes.message || parentes.comment || ""}
                  </p>
                </div>
              </div>
              <>
                {parentes?.reposts?.length > 0 &&
                  parentes.reposts.map((re: any) => {
                    return (
                      <div key={re.id}>
                        <RecursivePost
                          parent={[re]}
                          name="Repost"
                        ></RecursivePost>
                      </div>
                    );
                  })}
              </>
              <>
                {parentes?.quotes?.length > 0 &&
                  parentes.quotes.map((re: any) => {
                    return (
                      <div key={re.id}>
                        <RecursivePost parent={re} name="Quote"></RecursivePost>
                      </div>
                    );
                  })}
              </>
            </div>
          );
        })}
    </>
  );
}
