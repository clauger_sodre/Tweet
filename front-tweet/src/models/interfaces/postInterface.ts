export interface Quote {
  id: number;
  data: Date;
  comment: string;
  createdAt: Date;
  postId?: number;
  repostId?: number;
  userId: number;
  reposts: Repost[]; // eslint-disable-line no-use-before-define
}

export interface Repost {
  id: number;
  data: Date;
  postId?: number;
  quoteId?: number;
  userId: number;
  quotes: Quote[];
}

export default interface Post {
  id: number;
  userId: number;
  data: Date;
  message: string;
  quotes: Quote[];
  reposts: Repost[];
}

export interface InitialUserData {
  joined?: string;
  id?: number;
  username?: string;
  email?: string;
  password?: string;
  createdAt?: Date;
  updatedAt?: Date;
  posts?: Post[];
}
