import Layout from "@/components/layout";
import { StateRedux } from "@/store/type-redux";
import { useDispatch, useSelector } from "react-redux";

export default function CreateTweet() {
  const dispatch = useDispatch();
  const numberOfPosts = useSelector((state: StateRedux) => state.numberOfPosts);

  function handlePost() {
    dispatchPost(numberOfPosts + 1);
  }
  const dispatchPost = (post: number) => {
    dispatch({
      type: "NUMBER_POSTS",
      numberOfPosts: post,
    });
  };
  function handleDecremetPost() {
    if (numberOfPosts > 0) {
      dispatchPost(numberOfPosts - 1);
    }
  }

  return (
    <Layout>
      <h1> 🐦 {numberOfPosts}</h1>
      <button onClick={handlePost}>AddPost</button>
      <button onClick={handleDecremetPost}>Decrement Post</button>
    </Layout>
  );
}
