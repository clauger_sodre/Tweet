import { useDispatch } from "react-redux";
import HomePage from "@/components/homePage";
import Layout from "@/components/layout";
import { InitialUserData } from "@/models/interfaces/postInterface";

export default function Home({ data }: { data: InitialUserData }) {
  const dispatch = useDispatch();
  const posts = data.posts;
  delete data.posts;
  const userData = data;
  const numberOfPosts = posts?.length || 0;

  // Dispatching data to Redux
  dispatch({
    type: "USER_DATA",
    userData,
  });

  dispatch({
    type: "LANGUAGE",
    language: {
      languageSelected: "pt-BR",
    },
  });
  dispatch({
    type: "POST",
    post: posts,
  });
  dispatch({
    type: "NUMBER_POSTS",
    numberOfPosts,
  });
  data.posts = posts;
  return (
    <>
      <Layout>
        <HomePage props={data} />
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  let url = "";
  // Configuring BackEnd endpoint
  typeof process.env.IP_BACKEND !== "undefined"
    ? (url = `http://${process.env.IP_BACKEND}:3001/users/1/posts`)
    : (url = "http://localhost:3001/users/1/posts");
  const body = {};
  // Fetch data from external API
  try {
    const res = await fetch(url, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(body), // body data type must match "Content-Type" header
    });
    const data = await res.json();
    // Pass data to the page via props
    return { props: { data } };
  } catch (error) {
    console.log("Some error occurs", error);
  }
}
