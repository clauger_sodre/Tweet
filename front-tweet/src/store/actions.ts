import { PostRedux, Languages, Logout, UserDataRedux, NumberOfPosts } from "./type-redux";

export type Action = UserDataRedux | Languages | PostRedux | Logout |NumberOfPosts;
