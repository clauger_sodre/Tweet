import { StateRedux } from "@/store/type-redux";
import { Action } from "./actions";

const INITIAL_STATE: StateRedux = {
  userData: {},
  posts: [],
  numberOfPosts: 300,
  language: {
    languageSelected: "pt",
  },
};

// eslint-disable-next-line default-param-last
const userRedux = (
  state: StateRedux = INITIAL_STATE,
  action: Action
): StateRedux => {
  switch (action.type) {
    case "USER_DATA":
      return {
        ...state,
        userData: action.userData,
      };
    case "POST":
      return {
        ...state,
        posts: action.post,
      };

    case "LOGOUT":
      return INITIAL_STATE;

    case "LANGUAGE":
      return {
        ...state,
        language: action.language,
      };
    case "NUMBER_POSTS":
      return {
        ...state,
        numberOfPosts: action.numberOfPosts,
      };
    default:
      return state;
  }
};

export default userRedux;
