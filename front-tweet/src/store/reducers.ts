import {
  StateRedux,
  NUMBER_POSTS_TYPE,
  LANGUAGE_TYPE,
  USER_DATA_TYPE,
  POST_TYPE,
  LOGOUT_TYPE
} from "@/store/type-redux"
import { Action } from "./actions"

const INITIAL_STATE: StateRedux = {
  userData: {},
  posts: [],
  numberOfPosts: 300,
  language: {
    languageSelected: "pt",
  },
}

// eslint-disable-next-line default-param-last
const userRedux = ( // UserReducer
  state: StateRedux = INITIAL_STATE,
  action: Action
): StateRedux => {
  switch (action.type) {
    case USER_DATA_TYPE:
      return {
        ...state,
        userData: action.userData,
      }
    case POST_TYPE:
      return {
        ...state,
        posts: action.post,
      }

    case LOGOUT_TYPE:
      return INITIAL_STATE

    case LANGUAGE_TYPE:
      return {
        ...state,
        language: action.language,
      }
    case NUMBER_POSTS_TYPE:
      return {
        ...state,
        numberOfPosts: action.numberOfPosts,
      }
    default:
      return state
  }
}

export default userRedux
