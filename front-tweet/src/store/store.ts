import { createStore } from 'redux';
import userRedux from '.';

const store = createStore(userRedux);

console.log(store.getState())
export default store;