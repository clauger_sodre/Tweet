import Post from "@/models/interfaces/postInterface"
export const NUMBER_POSTS_TYPE = "NUMBER_POSTS"
export const LANGUAGE_TYPE = "LANGUAGE"
export const USER_DATA_TYPE = "USER_DATA"
export const POST_TYPE = "POST"
export const LOGOUT_TYPE = "LOGOUT"

export interface UserData {
  joined?: string
  id?: number
  username?: string
  email?: string
  password?: string
  createdAt?: Date
  updatedAt?: Date
}
export interface StateRedux {
  userData: UserData
  posts: Post[]
  numberOfPosts: number
  language: {
    languageSelected: string
  }
}
export interface NumberOfPosts {
  type: "NUMBER_POSTS"
  numberOfPosts: number
}
export interface Languages {
  type: "LANGUAGE"
  language: {
    languageSelected: string
  }
}
export interface UserDataRedux {
  type: "USER_DATA"
  userData: UserData
}
export interface PostRedux {
  type: "POST"
  post: Post[]
}
export interface Logout {
  type: "LOGOUT"
}

