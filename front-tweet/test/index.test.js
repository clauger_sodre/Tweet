import Home from '../pages/index'
import '@testing-library/jest-dom'
import { fireEvent, render, screen } from '@testing-library/react'
describe('Home', () => {
  it('renders the homePage', () => {
    render(<Home />)
    // check if all components are rendered
    expect(screen.getByTestId('result')).toBeInTheDocument()
    /*
    fireEvent.change(num1input, { target: { value: 5 } })
    fireEvent.change(num2input, { target: { value: 8 } })
    addButton.click()
    expect(resultArea).toHaveTextContent('13')
    */
  })
})
